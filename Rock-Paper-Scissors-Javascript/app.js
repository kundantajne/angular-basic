let userScore = 0;
let computerScore = 0;
const userScore_span = document.getElementById("user-score");
const computerScore_span = document.getElementById("computer-score");
const scoreBoard_div = document.querySelector("score-board");
const result_p = document.querySelector(".result > p");
const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissors_div = document.getElementById("s");
const reset_div = document.getElementById("reset");

function getcomputerChoice() {
    const choices = ["r", "p", "s"]
    const randomNumber = Math.floor(Math.random() * 3);
    return choices[randomNumber];
}

function convertToWord(letter) {
    if (letter === "r") return "Rock";
    if (letter === "p") return "Paper";
    return "Scissors";
}

function win(userChoice, computerChoice) {
    userScore++;
    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    const smallUserWord = "User".fontsize(3).sup();
    const smallCompWord = "Comp".fontsize(3).sup();

    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord} beats ${convertToWord(computerChoice)}${smallCompWord} .You Win!`;

}
function lose(userChoice, computerChoice) {
    computerScore++;
    computerScore_span.innerHTML = computerScore;
    userScore_span.innerHTML = userScore;

    const smallUserWord = "User".fontsize(3).sup();
    const smallCompWord = "Comp".fontsize(3).sup();

    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord} loses to ${convertToWord(computerChoice)}${smallCompWord} .You Lost`;


}
function draw(userChoice, computerChoice) {
    const smallUserWord = "User".fontsize(3).sup();
    const smallCompWord = "Comp".fontsize(3).sup();

    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord} equal to ${convertToWord(computerChoice)}${smallCompWord} .Its Draw`;

}
function reset(userChoice) {
    userScore = 0;
    computerScore = 0;
    computerScore_span.innerHTML = computerScore;
    userScore_span.innerHTML = userScore;
    result_p.innerHTML = `Lets Play Again!`

}


function game(userChoice) {
    let computerChoice = ""
    if (userChoice != 'reset')
        computerChoice = getcomputerChoice();


    switch (userChoice + computerChoice) {
        case "rs":
        case "pr":
        case "sp":
            win(userChoice, computerChoice);
            break;
        case "rp":
        case "ps":
        case "sr":
            lose(userChoice, computerChoice);
            break;

        case "rr":
        case "pp":
        case "ss":
            draw(userChoice, computerChoice);
            break;

        case "reset":

            reset(userChoice);


            break;

    }

}


function test() {
    rock_div.addEventListener('click', function () {

        game('r')

    })
    paper_div.addEventListener('click', function () {
        game('p')
    })
    scissors_div.addEventListener('click', function () {
        game('s')
    })
    reset_div.addEventListener('click', function () {
        console.log(reset_div)
        game('reset')

    })
}
test();