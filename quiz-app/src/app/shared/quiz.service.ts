import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class QuizService {
  //------------------ class properties-----------------------
  readonly rooturl = 'http://localhost:50445/'
  qns: any[];
  seconds: number;
  timer;
  qnProgress: number;



  //--------------------Helper Method------------


  constructor(private Http: HttpClient) { }

  displayTimeElapsed() {
    return Math.floor(this.seconds / 3600) + ':' + Math.floor(this.seconds / 60) + ':' + Math.floor(this.seconds % 60);
  }

  //----------------Http Method------------------------

  insertParticipant(name: string, email: string) {
    var body = {
      Name: name,
      Email: email
    }
    return this.Http.post(this.rooturl + 'api/InsertParticipant', body)
  }

  GetQuestions() {
    return this.Http.get(this.rooturl + "api/Questions");
  }

  Answer(qID, choice) {

  }
}
