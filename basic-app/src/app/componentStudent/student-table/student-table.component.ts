import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-table',
  templateUrl: './student-table.component.html',
  styleUrls: ['./student-table.component.css']
})
export class StudentTableComponent implements OnInit {
  getinput = false;
  empty() {
    this.student = {
      rollno: "",
      name: "",
      branch: ""

    }
  }
  student: any = {}

  students = [{
    rollno: 51,
    name: "Kundan",
    branch: "comp"
  },
  {
    rollno: 52,
    name: "Kiran",
    branch: "comp"
  }
  ]
  constructor() {

  }

  addStudent() {
    this.getinput = true;
    if (this.student.rollno && this.student.name && this.student.branch) {
      this.students.push(this.student)
      this.empty();
      this.getinput = false;
    }

    console.log("empty");
  }

  ngOnInit() {
  }

}
