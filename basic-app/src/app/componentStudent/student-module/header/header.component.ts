import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  result: object;
  constructor() {
    this.result = JSON.parse(localStorage.getItem("login"));
    if (this.result) {
      console.log("you already login");
    }
  }

  ngOnInit() {
  }

}
