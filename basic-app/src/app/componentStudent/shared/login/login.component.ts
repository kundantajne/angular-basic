import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  result: object;
  users = [
    {
      userId: "a",
      password: "kundan",
      Name: "Kundan"

    },
    {
      userId: "b",
      password: "abc",
      Name: "Kiran"

    },


  ]

  constructor() {

  }
  username: string;
  password: string;

  onLogin() {
    if (this.username && this.password) {
      let user = this.users.filter(x => x.userId == this.username && x.password == this.password)[0]
      if (user) {

        localStorage.setItem("login", JSON.stringify(user))
        console.log("login success");
      }
      else
        console.log("invalid user");
    }
    else
      console.log("empty");
  }
  onClear() {
    this.username = "";
    this.password = "";

  }

  ngOnInit() {
  }

}
