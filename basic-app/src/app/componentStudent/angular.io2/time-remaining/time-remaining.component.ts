import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-time-remaining',
  templateUrl: './time-remaining.component.html',
  styleUrls: ['./time-remaining.component.css']
})
export class TimeRemainingComponent implements OnInit {
  @Input() time = "60 sec";

  constructor() { }

  ngOnInit() {
  }

}
