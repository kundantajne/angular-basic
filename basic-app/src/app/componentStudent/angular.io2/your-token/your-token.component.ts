import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-your-token',
  templateUrl: './your-token.component.html',
  styleUrls: ['./your-token.component.css']
})
export class YourTokenComponent implements OnInit {
  @Input() token = 12345;

  constructor() { }

  ngOnInit() {
  }

}
