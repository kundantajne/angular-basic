import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourTokenComponent } from './your-token.component';

describe('YourTokenComponent', () => {
  let component: YourTokenComponent;
  let fixture: ComponentFixture<YourTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
