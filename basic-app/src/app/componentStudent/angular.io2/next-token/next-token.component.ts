import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-next-token',
  templateUrl: './next-token.component.html',
  styleUrls: ['./next-token.component.css']
})
export class NextTokenComponent implements OnInit {
  @Output() onBtnClick: EventEmitter<any> = new EventEmitter()
  newToken($event) {
    console.log('Token Change');
    this.onBtnClick.next("data")
  }

  constructor() { }

  ngOnInit() {
  }

}
