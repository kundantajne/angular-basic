import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NextTokenComponent } from './next-token.component';

describe('NextTokenComponent', () => {
  let component: NextTokenComponent;
  let fixture: ComponentFixture<NextTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NextTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NextTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
