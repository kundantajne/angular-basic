import { Component, OnInit } from '@angular/core';
import { AserviceService } from 'src/app/services/aservice.service';
import { CustomHttpService } from 'src/app/services/custom-http.service';


@Component({
  selector: 'app-authenticator',
  templateUrl: './authenticator.component.html',
  styleUrls: ['./authenticator.component.css']
})
export class AuthenticatorComponent implements OnInit {
  myToken = Math.floor(Math.random() * 10000 + 1);
  remtime = 60
  getData($event) {
    this.myToken = Math.floor(Math.random() * 10000 + 1)
    this.remtime = 60

  }
  constructor(private dal: AserviceService, private customHttp: CustomHttpService) {
    console.log(dal.getUserInfo());
    console.log(dal.getToken());
    console.log(dal.getCurrentDate());
    console.log(dal.getTimerToken());
    dal.getCallBackTimer((x) => {
      console.log(x)

    });
    dal.getObservable();
    dal.getSubject();
    dal.getBehaviourSub();
    dal.getProject().subscribe(x => {
      console.log("yo", x);
    })


    setInterval(() => {
      this.remtime--
    }, 1 * 1000)

  }


  ngOnInit() {
  }

}
