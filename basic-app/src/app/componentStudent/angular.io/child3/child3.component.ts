import { Component, OnInit, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-child3',
  templateUrl: './child3.component.html',
  styleUrls: ['./child3.component.css']
})
export class Child3Component implements OnInit {
  @Output() onBtnClick: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  toSend() {
    this.onBtnClick.next("Kundan here");


  }




}
