import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title;
  arr = ["My Task 1", "My Task 2", "My Task 3", "My Task 4", "my Task 5"];
  newTask;
  constructor() {
    setInterval(() => {
      this.title = new Date();
    }, 1000)
  }
  addNewTask() {
    this.arr.push(this.newTask);
    this.newTask = "";
  }
  removeTask(i) {
    alert(i);
    this.arr.splice(i, 1);
  }
}
