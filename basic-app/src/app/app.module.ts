import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StudentTableComponent } from './componentStudent/student-table/student-table.component';
import { HeaderComponent } from './componentStudent/student-module/header/header.component';
import { BranchComponent } from './componentStudent/student-module/branch/branch.component';
import { StudentComponent } from './componentStudent/student-module/student/student.component';
import { FooterComponent } from './componentStudent/student-module/footer/footer.component';
import { LayoutComponent } from './componentStudent/student-module/layout/layout.component';
import { LoginComponent } from './componentStudent/shared/login/login.component';
import { RegisterComponent } from './componentStudent/shared/register/register.component';
import { BlankLayoutComponent } from './componentStudent/layout/blank-layout/blank-layout.component';
import { Router, RouterModule, Routes } from '@angular/router';
import { ParentComponent } from './componentStudent/angular.io/parent/parent.component';
import { Child1Component } from './componentStudent/angular.io/child1/child1.component';
import { Child2Component } from './componentStudent/angular.io/child2/child2.component';
import { Child3Component } from './componentStudent/angular.io/child3/child3.component';
import { TimeRemainingComponent } from './componentStudent/angular.io2/time-remaining/time-remaining.component';
import { YourTokenComponent } from './componentStudent/angular.io2/your-token/your-token.component';
import { NextTokenComponent } from './componentStudent/angular.io2/next-token/next-token.component';
import { AuthenticatorComponent } from './componentStudent/angular.io2/authenticator/authenticator.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';


const routes: Routes = [
  {
    path: "login", component: LoginComponent


  },
  {
    path: "layout", component: LayoutComponent
  },
  {
    path: "register", component: RegisterComponent
  },
  {
    path: "", component: BlankLayoutComponent
  },
  {
    path: "parent", component: ParentComponent
  },
  {
    path: "auth", component: AuthenticatorComponent
  },
  {
    path: "student", component: StudentComponent

  }


]
@NgModule({
  declarations: [
    AppComponent,
    StudentTableComponent,
    HeaderComponent,
    BranchComponent,
    StudentComponent,
    FooterComponent,
    LayoutComponent,
    LoginComponent,
    RegisterComponent,
    BlankLayoutComponent,
    ParentComponent,
    Child1Component,
    Child2Component,
    Child3Component,
    AuthenticatorComponent,
    TimeRemainingComponent,
    YourTokenComponent,
    NextTokenComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,

    HttpClientModule,

    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
