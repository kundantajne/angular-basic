import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './main/home/home.component';
import { BlankLayoutComponent } from './blank-layout/blank-layout.component';

const routes: Routes = [{ path: "", component: BlankLayoutComponent },
{ path: "home", component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
