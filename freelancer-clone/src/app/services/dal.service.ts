import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CustomHttpService } from './custom-http.service';

@Injectable({
  providedIn: 'root'
})
export class DalService {

  constructor(private customhttp: CustomHttpService) { }
  getMenu1() {
    return this.customhttp.getReq1("http://localhost:3000/menu")
  }
  getNotification() {
    return this.customhttp.getReq1("http://localhost:3000/notification")
  }
}