import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomHttpService {

  constructor(private httpclient: HttpClient) { }
  getReq1(url: string, query?: any) {
    return this.httpclient.get(url, query);
  }
}
