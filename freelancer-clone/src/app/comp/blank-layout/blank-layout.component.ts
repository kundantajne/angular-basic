import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'proj-blank-layout',
  templateUrl: './blank-layout.component.html',
  styleUrls: ['./blank-layout.component.css']
})
export class BlankLayoutComponent implements OnInit {
  isHome = false;
  constructor(private router: Router) {
    this.router.events.subscribe((x: NavigationStart) => {
      if (x["urlAfterRedirects"] == "/") {
        this.isHome = true;

      }
      else {
        this.isHome = false;
      }
    })

  }

  ngOnInit() {
  }

}
