import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from '../../people/layout/layout.component';
import { ProfileComponent } from '../../people/profile/profile.component';
import { SearchComponent } from '../../people/search/search.component';
import { AppRoutingModule } from 'src/app/app-routing.module';

@NgModule({
  declarations: [
    LayoutComponent,
    ProfileComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule

  ],
  exports: [
    LayoutComponent,
    ProfileComponent,
    SearchComponent
  ]
})
export class PeopleModule { }
