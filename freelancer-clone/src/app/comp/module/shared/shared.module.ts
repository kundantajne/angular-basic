import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from '../../shared/header/header.component';
import { FooterComponent } from '../../shared/footer/footer.component';
import { HireoLayoutComponent } from '../../shared/hireo-layout/hireo-layout.component';
import { AppRoutingModule } from 'src/app/app-routing.module';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    HireoLayoutComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule

  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    HireoLayoutComponent
  ]
})
export class SharedModule {

}
