import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjDetailsComponent } from '../../project/proj-details/proj-details.component';
import { ProjListComponent } from '../../project/proj-list/proj-list.component';
import { ProjLayoutComponent } from '../../project/proj-layout/proj-layout.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
// import { ProjTaskComponent } from '../../project/proj-task/proj-task.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { BlankLayoutComponent } from '../../blank-layout/blank-layout.component';



@NgModule({
  declarations: [
    ProjDetailsComponent,
    ProjListComponent,
    ProjLayoutComponent,

  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: "", component: BlankLayoutComponent, children: [{
          path: "project", component: ProjLayoutComponent,
          children: [
            { path: "list", component: ProjListComponent },
            {
              path: "details", component: ProjDetailsComponent
            }
          ]
        },]
      },
      { path: "**", redirectTo: "/projects/list" }
    ]
    )],
  exports: [
    ProjDetailsComponent,
    ProjListComponent,
    ProjLayoutComponent,

    // ProjTaskComponent


  ]
})
export class ProjectModule { }
