import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjLayoutComponent } from './proj-layout.component';

describe('ProjLayoutComponent', () => {
  let component: ProjLayoutComponent;
  let fixture: ComponentFixture<ProjLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
