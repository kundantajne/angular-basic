import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjTaskComponent } from './proj-task.component';

describe('ProjTaskComponent', () => {
  let component: ProjTaskComponent;
  let fixture: ComponentFixture<ProjTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
