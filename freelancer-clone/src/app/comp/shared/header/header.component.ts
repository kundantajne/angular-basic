import { Component, OnInit } from '@angular/core';
import { DalService } from 'src/app/services/dal.service';

@Component({
  selector: 'proj-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  obj: any;
  // obj = [{
  //   title: "frelancer",
  //   children: [
  //     {
  //       title: "find work",
  //       rLink: "/project/details"
  //     },
  //     {
  //       title: "Projects",
  //       rLink: "/project/list"
  //     },
  //     {
  //       title: "Tasks",
  //       rLink: "/project/task"
  //     }

  //   ]
  // }, {
  //   title: "Employers",
  //   children: [
  //     {
  //       title: "find Freelancer"
  //     },
  //     {
  //       title: "Projects"
  //     }
  //   ]
  // }]
  constructor(private dal: DalService) {
    this.dal.getMenu1().subscribe(x => {
      this.obj = x
    })
  }


  ngOnInit() {
  }

}
