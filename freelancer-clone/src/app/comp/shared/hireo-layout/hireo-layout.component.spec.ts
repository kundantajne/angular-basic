import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HireoLayoutComponent } from './hireo-layout.component';

describe('HireoLayoutComponent', () => {
  let component: HireoLayoutComponent;
  let fixture: ComponentFixture<HireoLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HireoLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HireoLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
