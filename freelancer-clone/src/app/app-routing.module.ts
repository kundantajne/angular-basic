import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './comp/people/layout/layout.component';
import { HireoLayoutComponent } from './comp/shared/hireo-layout/hireo-layout.component';
import { ProfileComponent } from './comp/people/profile/profile.component';
import { SearchComponent } from './comp/people/search/search.component';
import { BlankLayoutComponent } from './comp/blank-layout/blank-layout.component';


const routes: Routes = [
  {
    path: "", component: BlankLayoutComponent
  },
  { path: "a", loadChildren: "./comp/module/project/project.module#ProjectModule" },
  {

    path: "people", component: LayoutComponent,
    children: [
      {
        path: "profile", component: ProfileComponent
      },
      {
        path: "search", component: SearchComponent
      }
    ]


  },
  {
    path: "hireo", component: HireoLayoutComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
