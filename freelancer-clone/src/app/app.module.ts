import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './comp/module/shared/shared.module';
// import { ProjectModule } from './comp/module/project/project.module';
import { PeopleModule } from './comp/module/people/people.module';
import { ProjTaskComponent } from './comp/project/proj-task/proj-task.component';
import { LandingComponent } from './comp/landing/landing.component';

import { HttpClientModule } from "@angular/common/http"

import { BlankLayoutComponent } from './comp/blank-layout/blank-layout.component';


@NgModule({
  declarations: [
    AppComponent,
    ProjTaskComponent,
    LandingComponent,
    BlankLayoutComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    // ProjectModule,
    PeopleModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
